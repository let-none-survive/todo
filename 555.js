let $newtodo = $('#newTodo');
var clear = $('#DeleteAllItems')
$(document).ready(function() {
    $('#addItem').on('click', addItem);
    $('#todos').on('change', '.completeItem', CompleteItem);
    $('#todos').on('click', '.deleteItem', DeleteItem);
    $('#todos').on('dblclick', '.todoText', stratEditing);
    $('#todos').on('click', '.saveItem', stopEditing);
    $('#newTodo').on('keypress', function(event) {
        if (event.which === 13) {
            addItem();
            event.preventDefault();
        }
    });

    function stratEditing(event) {
        var Li = $(this).parent();
        var currentText = Li.find('.todoText').text();
        Li.find('.editText').val(currentText);
        Li.find('.editText').show();
        Li.find('.saveItem').show();
        Li.find('.todoText').hide();
    }

    function addItem(event) {
        var todoAddList = $('#newTodo').val();
        if ( !todoAddList ) {
        alert('Ничего нет в строке ввода!');
        }
        else {
          $('#todos').append('<li><input class="completeItem" type="checkbox"><span class="todoText">' + todoAddList + '</span><input type="text" class="editText"><button class="btn btn-success saveItem">save</button><i class="fa fa-trash deleteItem" aria-hidden="true"></i></li>');
          $("#newTodo").val('');
        }
    }

    function CompleteItem(event) {
        $(this).parent().toggleClass('done');
    }

    function DeleteItem(event) {
        $(this).parent().remove();
    }

    function stopEditing(event) {
        var Li = $(this).parent();
        $(this).hide();
        var newVal = Li.find('.editText').val();
        Li.find('.editText').hide();
        Li.find('.todoText').text(newVal);
        Li.find('.todoText').show();
    }
    $('#DeleteAllItems').on('click', function() {
        $('#todos').children().remove();
    });
    $('#Done').on('click', function() {
        $('#todos').children().toggleClass('done');
    });
});
